package ru.t1.kharitonova.tm.command.project;

import org.jetbrains.annotations.NotNull;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Clear project.";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECTS CLEAR]");
        @NotNull final String userId = getUserId();
        getProjectService().removeAllByUserId(userId);
    }

}
