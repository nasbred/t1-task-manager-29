package ru.t1.kharitonova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kharitonova.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "Remove task by index.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-remove-by-index";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getUserId();
        getTaskService().removeOneByIndex(userId, index);
    }

}
